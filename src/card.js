'use strict';

const arr = [1,2,3,4,5,6,7,8,9];

// method for render dynamic html
const render = (arr) => {
    let content = document.getElementById("shuffle");
    let list = "<div class='wrapper'>";
    for(let i = 0; i < arr.length; i++ ) {          
        list+= `<div class='card${arr[i]}'> ${arr[i]} </div>`; 
    }
    list += "</div>";
    content.innerHTML=list;
}

// method for gettings items in random shuffle order
const getShuffle = (arr) => {
    return [...arr].map( (_, i, arrCopy) => {
        var rand = i + ( Math.floor( Math.random() * (arrCopy.length - i)));
        [arrCopy[rand], arrCopy[i]] = [arrCopy[i], arrCopy[rand]]
        return arrCopy[i]
    })
}

// invoking methods click on Shuffle button 
const onShuffle = () => {    
   let array = getShuffle(arr);
   render(array);  
}

// method for getting items sort in ascending order
const getSort = (arr) => {
    return arr.sort(function(a, b){
        return a - b;
    });
}

// invoking methods click on Sort button
const onSort = () => {
    getSort(arr);
    render(arr);
}
